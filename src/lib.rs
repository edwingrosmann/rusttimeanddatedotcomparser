#![warn(warnings)]
#![macro_use]
#![feature(const_fn)]
pub mod animation;
pub mod clock_widget;
pub mod data_api;
pub mod druid_clock_app;
pub mod druid_ui;
pub mod kafka_api;
pub mod mongo_api;
pub mod parse_timeanddate_dot_com;
pub mod settings;

#[cfg(test)]
mod tests;

/// In order for this binary crate to run integration tests, both lib.rs AND main.rs have to be present.
/// main::main() call this function.
/// See more on this topic: https://doc.rust-lang.org/book/ch11-03-test-organization.html#integration-tests-for-binary-crates
pub fn main(test_mode: bool, args: Vec<String>) {
    settings::set_args(args);
    settings::set_test_mode(test_mode);

    druid_ui::show(data_api::fetch_time_data());
    druid_clock_app::show();
    animation::show_animation();
}
