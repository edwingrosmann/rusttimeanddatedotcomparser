use bson;
use bson::from_bson;
use chrono::Utc;
use mongodb::bson::{doc, Bson, Document};
use mongodb::options::FindOptions;
use mongodb::Client;
use tokio::stream::StreamExt;

use crate::parse_timeanddate_dot_com::TimeData;

const ID: &'static str = "_id";

pub fn select_by_keys(keys: Vec<String>) -> Vec<TimeData> {
    let client = create_mongo_client("localhost").expect("Could not create aMongoDB Client");
    let start = Utc::now();
    let data = read_some(keys, &client, "test", "city_data")
        .expect("Error Occurred whilst reading all time-data");

    println!(
        "Loading-time for all City-data from cache: {}",
        Utc::now() - start
    );
    data
}

///When storing freshly downloaded time-data, the existing data will be dropped; this will ensure that
/// old pages that are now longer downloaded/updated - because of a change in pages-to-download - do not
/// linger and thusly are inaccurate. Also it prevents no-longer-wanted-pages to show up in the UI.
pub fn store(new_data: &Vec<TimeData>) {
    //Create a MongoDB-client...
    let client = create_mongo_client("localhost").expect("Could not create a MongoDB Client");

    let start = Utc::now();

    //Store all the cities...
    for page_data in new_data {
        // let s = serde_json::to_value(city).unwrap();
        // let city_doc = &mut serde_json::from_value(s).expect("Creating City-data-document failed");
        upsert(
            &client,
            "test",
            "city_data",
            &mut bson::to_document(&page_data)
                .expect("Could not create Document from City-data")
                .clone(),
        )
        .expect(&format!(
            "Error Occurred whilst storing City-data {:?}",
            page_data
        ));
    }
    println!("Storing-time for all City-data: {}", Utc::now() - start);
}

#[tokio::main]
async fn create_mongo_client(host: &str) -> mongodb::error::Result<Client> {
    Ok(Client::with_uri_str(format!("mongodb://{}:27017", host).as_str()).await?)
}

#[tokio::main]
async fn upsert(
    client: &Client,
    database: &str,
    collection_name: &str,
    data: &mut Document,
) -> mongodb::error::Result<()> {
    let collection = client.database(database).collection(collection_name);

    //Find by key-field...
    let q = &doc! {"_id":data.get_str("_id").unwrap()};

    //Upsert: If the document exists then update otherwise insert.
    //Empirical Observation: Updating and inserting takes about the same...
    if collection.count_documents(q.clone(), None).await? > 0 {
        collection.update_one(q.clone(), data.clone(), None).await?;
        println!(
            "Updated New City-time with key {:?} into collection: {}/{}",
            get_id(data),
            database,
            collection_name
        );
    } else {
        println!(
            "Inserted New City-time with key {} into collection: {}/{}",
            collection.insert_one(data.clone(), None).await?.inserted_id,
            database,
            collection_name
        );
    }
    Ok(())
}

#[tokio::main]
async fn read_some(
    keys: Vec<String>,
    client: &Client,
    database: &str,
    collection_name: &str,
) -> mongodb::error::Result<Vec<TimeData>> {
    let collection = client.database(database).collection(collection_name);

    //Find by keys...

    let mut all_time_data = collection
        .find(doc! {"_id":{"$in":keys}}, options())
        .await?;
    let mut result = Vec::<TimeData>::new();

    while let Some(doc) = all_time_data.next().await {
        let d = doc.unwrap();
        let bs = Bson::from(&d);
        let td = from_bson::<TimeData>(bs)
            .expect("Mongodb Document could not be converted, to TimeData");
        result.push(td);
    }
    println!(
        "MongoDb Found Documents with Keys: {:?}:",
        result.iter().map(TimeData::get_id).collect::<Vec<String>>()
    );

    result.sort_by_key(|t| t._id.to_string());

    Ok(result)
}

fn options() -> FindOptions {
    FindOptions::builder().sort(doc! {"_id": 1}).build()
}

fn get_id(data: &Document) -> &str {
    data.get_str(ID).unwrap()
}
