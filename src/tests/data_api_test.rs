use std::collections::HashMap;

use chrono::{Duration, Utc};

use crate::data_api::cache_status;
use crate::parse_timeanddate_dot_com::TimeData;
use crate::settings::sort_strategy;

#[test]
fn cache_valid_test() {
    //No cache, no urls...
    let data = &mut Vec::default();
    let urls = &mut HashMap::default();
    let sort = sort_strategy();

    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //1 cache; 0 urls: unequal count...
    data.push(TimeData {
        _id: "Key1".to_string(),
        last_updated: Utc::now().to_rfc3339(),
        sort: sort,
        ..Default::default()
    });
    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //1 cache; 1 urls: equal count, but url-key not found in cache...
    urls.insert(String::from("Key2"), String::from("Url1"));
    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //1 cache; 2 urls: unequal equal count, but url-key IS found in cache...
    urls.insert(String::from("Key1"), String::from("Url1"));
    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //2 cache; 2 urls: equal equal count, AND url-keys ARE all found in cache...
    data.push(TimeData {
        _id: "Key2".to_string(),
        last_updated: Utc::now().to_rfc3339(),
        sort: sort,
        ..Default::default()
    });
    assert_eq!(cache_status(data, urls, &Vec::default()), true);

    //3 cache; 2 urls: unequal equal count, AND url-keys ARE all found in cache...
    data.push(TimeData {
        _id: "Key3".to_string(),
        last_updated: Utc::now().to_rfc3339(),
        sort: sort,
        ..Default::default()
    });
    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //3 cache; 3 urls: equal equal count, AND url-keys ARE all found in cache...
    urls.insert(String::from("Key3"), String::from("Url1"));
    assert_eq!(cache_status(data, urls, &Vec::default()), true);

    //Invalidate cache by making a the last_updated field in a the cached item older than 480 minutes ago
    data.remove(2);
    data.insert(
        2,
        TimeData {
            _id: "Key3".to_string(),
            last_updated: (Utc::now() - Duration::minutes(8 * 60 + 1)).to_rfc3339(),
            sort: sort,
            ..Default::default()
        },
    );
    assert_eq!(cache_status(data, urls, &Vec::default()), false);

    //Validate cache by making a the last_updated field in a the cached item one second less than 480 minutes ago
    data.remove(2);
    data.insert(
        2,
        TimeData {
            _id: "Key3".to_string(),
            last_updated: (Utc::now() - Duration::minutes(8 * 60) + Duration::seconds(1))
                .to_rfc3339(),
            sort: sort,
            ..Default::default()
        },
    );
    assert_eq!(cache_status(data, urls, &Vec::default()), true);
}
