use crate::settings::*;
use crate::tests::args_for;
use chrono::Duration;
use std::collections::btree_set::BTreeSet;

#[test]
fn urls_from_test() {
    assert_eq!(
        urls_from("./src/tests/test-urls.txt")
            .iter()
            .map(|(k, v)| format!("{}={}|", k, v))
            .collect::<BTreeSet<String>>().iter()
            .map(|s| s.as_str())
            .collect::<String>(),
        "Afrika=A|Asia=A|Australasia=A|Europa=E|North Americas=N|Popular Cities=P|South Americas=S|".to_string()
    );
}

#[test]
fn some_urls_commented_out_test() {
    assert_eq!(
        urls_from("./src/tests/test-urls-some-commented-out.txt")
            .iter()
            .map(|(k, v)| format!("{}={}|", k, v))
            .collect::<BTreeSet<String>>()
            .iter()
            .map(|s| s.as_str())
            .collect::<String>(),
        "Afrika=A|Asia=A|Europa=E|North Americas=N|South Americas=S|".to_string()
    );
}

#[test]
fn ttl_arg_test() {
    set_args(args_for(vec!["ttl=123"]));
    assert_eq!(ttl(), Duration::minutes(123));
    set_args(args_for(vec!["ttl=124"]));
    assert_eq!(ttl(), Duration::minutes(124));
    set_args(args_for(vec!["TTL=125"]));
    assert_eq!(ttl(), Duration::minutes(125));
    set_args(args_for(vec!["Ttl=126"]));
    assert_eq!(ttl(), Duration::minutes(126));
    set_args(args_for(vec!["Ttl=1.01e2"]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));
    set_args(args_for(vec!["Bottle=127"]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));

    //The default when ttl parameter is not found...
    set_args(args_for(vec!["oopsie=124"]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));

    //Incorrect ttl value...
    set_args(args_for(vec!["ttl=oopsie"]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));

    //No ttl value...
    set_args(args_for(vec!["ttl"]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));
    //No ttl value...
    set_args(args_for(vec!["ttl="]));
    assert_eq!(ttl(), Duration::minutes(DEFAULT_TTL));
}

#[test]
fn do_cache_test() {
    set_args(args_for(vec!["cache"]));
    assert_eq!(do_cache(), true);
    set_args(args_for(vec!["Cache"]));
    assert_eq!(do_cache(), true);
    set_args(args_for(vec!["cache-please"]));
    assert_eq!(do_cache(), true);
    set_args(args_for(vec!["bibbidi Cache "]));
    assert_eq!(do_cache(), true);
    set_args(args_for(vec!["Kesh"]));
    assert_eq!(do_cache(), false);
    set_args(args_for(vec![]));
    assert_eq!(do_cache(), false);
}

#[test]
fn print_node_data_test() {
    set_args(args_for(vec!["print-node-data"]));
    assert_eq!(print_node_data(), true);
    set_args(args_for(vec!["Print-Node-data"]));
    assert_eq!(print_node_data(), true);
    set_args(args_for(vec!["Prunt-node-data"]));
    assert_eq!(print_node_data(), false);
    set_args(args_for(vec![]));
    assert_eq!(print_node_data(), false);
}

#[test]
fn do_use_kafka_test() {
    set_args(args_for(vec!["kafka"]));
    assert_eq!(do_use_kafka(), true);
    set_args(args_for(vec!["KAFka"]));
    assert_eq!(do_use_kafka(), true);
    set_args(args_for(vec!["do-not-use-kafka"]));
    assert_eq!(do_use_kafka(), true);
    set_args(args_for(vec!["bafka-schmafka"]));
    assert_eq!(do_use_kafka(), false);
}

#[test]
fn urls_file_name_test() {
    set_args(args_for(vec!["urls-file=test-urls.txt"]));
    assert_eq!(urls_file_name(), "test-urls.txt".to_string());
    set_args(args_for(vec!["urls-FILE=test2-urls.txt"]));
    assert_eq!(urls_file_name(), "test2-urls.txt".to_string());
    set_args(args_for(vec!["urls-file="]));
    assert_eq!(urls_file_name(), DEFAULT_URLS_FILE_NAME.to_string());
    set_args(args_for(vec![]));
    assert_eq!(urls_file_name(), DEFAULT_URLS_FILE_NAME.to_string());
}

#[test]
fn sort_by_name_test() {
    set_args(args_for(vec!["sort-by-name"]));
    assert_eq!(sort_strategy(), Sort::ByName);
    set_args(args_for(vec!["Order_by-Name"]));
    assert_eq!(sort_strategy(), Sort::ByName);
    set_args(args_for(vec!["Name-sorting"]));
    assert_eq!(sort_strategy(), Sort::ByName);
    set_args(args_for(vec!["name_Ordering"]));
    assert_eq!(sort_strategy(), Sort::ByName);
    set_args(args_for(vec!["bibbidi"]));
    assert_eq!(sort_strategy(), Sort::ByName);
}

#[test]
fn sort_by_offset_test() {
    set_args(args_for(vec!["sort-by-offset"]));
    assert_eq!(sort_strategy(), Sort::ByOffset);
    set_args(args_for(vec!["Order_by-offset"]));
    assert_eq!(sort_strategy(), Sort::ByOffset);
    set_args(args_for(vec!["offset-sorting"]));
    assert_eq!(sort_strategy(), Sort::ByOffset);
    set_args(args_for(vec!["offset-Ordering"]));
    assert_eq!(sort_strategy(), Sort::ByOffset);
    set_args(args_for(vec!["sort_by_offset"]));
    assert_eq!(sort_strategy(), Sort::ByOffset);
}

#[test]
fn kafka_topic_test() {
    set_args(args_for(vec!["kafka-topic=Schmoffic"]));
    assert_eq!(&kafka_topic(), "Schmoffic");
    set_args(args_for(vec!["topic=schmoffic"]));
    assert_eq!(&kafka_topic(), "schmoffic");
    set_args(args_for(vec!["KAFKA-TOPIC=schmofficK"]));
    assert_eq!(&kafka_topic(), "schmofficK");
    set_args(args_for(vec!["Topic=schmoFFic"]));
    assert_eq!(&kafka_topic(), "schmoFFic");
    set_args(args_for(vec!["topic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["kafka-topic="]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["topic="]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["Schmoppic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["kafka-topic:topic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec![]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
}
