mod data_api_test;
mod kafka_api_test;
mod parse_timeanddate_dot_com_tests;
mod settings_test;

pub fn args_for(args: Vec<&str>) -> Vec<String> {
    args.iter().map(|s| s.to_string()).collect()
}
