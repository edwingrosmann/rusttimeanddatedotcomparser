use std::fmt::Debug;
use std::process::Command as Cmd;
use std::time::Duration;

use kafka::producer::{Producer, Record, RequiredAcks};
use kafka::Result;
use serde::Serialize;

#[derive(Debug, Clone, Serialize)]
pub struct Key {
    pub name: String,
    pub action: KafkaAction,
}
#[derive(Debug, Clone, Serialize)]
pub enum KafkaAction {
    Upsert,
    Delete,
}

pub fn write<K: Debug + Serialize, T: Serialize>(key: K, data: T, topic: &str) -> Result<()> {
    create_topic(topic)?;

    println!("-Sending message on topic '{}' with key '{:?}'", topic, key);
    producer().send(&Record::from_key_value(
        topic,
        to_json(&key),
        to_json(&data),
    ))
}

fn producer() -> Producer {
    Producer::from_hosts(vec!["localhost:9092".to_owned()])
        .with_ack_timeout(Duration::from_secs(1))
        .with_required_acks(RequiredAcks::One)
        .create()
        .unwrap()
}

fn to_json<T: Serialize>(data: &T) -> String {
    bson::to_bson(&data)
        .expect("Could not Create Bson from data")
        .into_relaxed_extjson()
        .to_string()
}

/// Create the Kafka-topic; make sure that the confluent-bin-directory  - which contains the "kafka-topics" command that will be used by this function -
/// has been set in the $PATH environment variable:
/// e.g. in file  ```~/.profile```
/// add/update line to ```export PATH="$HOME/.cargo/bin:$HOME/confluent-5.3.1/bin:$PATH"```
pub fn create_topic(topic: &str) -> Result<()> {
    //Only continue when the topic does NOT yet exist...
    if producer().client().topics().contains(topic) {
        println!("Found Kafka-topic '{}'", topic);
        return Ok(());
    }

    match Cmd::new(format!("kafka-topics"))
        .arg("--create")
        .arg(format!("--topic={}", topic).as_str())
        .arg("--partitions=1")
        .arg("--replication-factor=1")
        .arg("--bootstrap-server=localhost:9092")
        .status()
    {
        Ok(_) => {
            println!("Created Kafka-topic: '{}'", topic);
        }
        Err(e) => {
            println!("Error creating Kafka-topic '{}': {}", topic, e);
        }
    }
    Ok(())
}
