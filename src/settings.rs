use std::cell::RefCell;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Result};
use std::str::FromStr;

use chrono::Duration;

pub use crate::parse_timeanddate_dot_com::Sort;

// A global variable, limited to the current Thread;
// In order to enable testing on the functions that probe the (immutable) env::args() collection, this contraption has been created:
// It is initialized with the env::args() collection and then the tests can add their test attributes to them...
thread_local!(
    static ARGS: RefCell<Vec<String>> = RefCell::new(Vec::new());
    static TEST_MODE: RefCell<bool> = RefCell::new(true);
);
pub const DEFAULT_TTL: i64 = 8 * 60;
pub const DEFAULT_TOPIC: &str = "time-data";
pub const DEFAULT_URLS_FILE_NAME: &str = "urls.txt";

pub fn set_args(new_args: Vec<String>) {
    ARGS.with(|args_cell| args_cell.replace(new_args));
    println!("Runtime Arguments: {:?}", args());
}

pub fn set_test_mode(test: bool) {
    TEST_MODE.with(|mode| mode.replace(test));
    println!("Test-mode: {}", test_mode());
}

pub fn args() -> Vec<String> {
    ARGS.with(|r| r.borrow().to_vec())
}

pub fn test_mode() -> bool {
    TEST_MODE.with(|m| m.to_owned().into_inner())
}

pub fn do_cache() -> bool {
    has_feature_flag("cache")
}

pub fn do_use_kafka() -> bool {
    has_feature_flag("kafka")
}

pub fn print_node_data() -> bool {
    has_feature_flag("print-node-data")
}

/// If an application parameter has been provided named something like 'sort-by-offset' or 'Sort_by_OFFSET' or 'order-by_Offset'
/// then ```Sort::ByOffset``` will be returned ```Sort::ByName``` otherwise
pub fn sort_strategy() -> Sort {
    match args().iter().any(|s| {
        (s.to_lowercase().contains("sort") || s.to_lowercase().contains("order"))
            && s.to_lowercase().contains("offset")
    }) {
        true => Sort::ByOffset,
        false => Sort::ByName,
    }
}

pub fn ttl() -> Duration {
    Duration::minutes(attribute_value("ttl", DEFAULT_TTL))
}

pub fn kafka_topic() -> String {
    attribute_value("topic", DEFAULT_TOPIC.to_string())
}

pub fn urls_file_name() -> String {
    attribute_value("urls-file", DEFAULT_URLS_FILE_NAME.to_string())
}

///For your Convenience.
pub fn urls() -> HashMap<String, String> {
    println!(
        "\n\nUsing URLS: from file '{}': {:?}\n\n",
        urls_file_name(),
        urls_from(&urls_file_name())
    );
    urls_from(&urls_file_name())
}

///Commented-out urls  - line starts with '//' or '#' - are ignored
pub fn urls_from(file_name: &str) -> HashMap<String, String> {
    reader(file_name)
        .lines()
        .filter(valid_line)
        .map(to_tuple)
        .collect()
}

fn reader(file_name: &str) -> BufReader<File> {
    BufReader::new(File::open(file_name)
        .expect(&format!("Please place file '{}' in the same directory as the executable; it contains the URLS to download the time-data from", file_name)))
}

///Valid lines are not commented out and do contain a 'key=value' pair.
fn valid_line(line: &Result<String>) -> bool {
    match line {
        Ok(line) => {
            line.trim().len() != 0
                && !line.trim().starts_with("#")
                && !line.trim().starts_with("//")
                && line.contains('=')
                && !line.trim().ends_with('=')
        }
        Err(_) => false,
    }
}

///Creates a name/URL tuple for the urls-hash-map...
fn to_tuple(l: Result<String>) -> (String, String) {
    let key_value = l
        .unwrap()
        .splitn(2, '=')
        .map(String::from)
        .collect::<Vec<String>>();

    (
        key_value[0].trim().to_owned(),
        key_value[1].trim().to_owned(),
    )
}

fn has_feature_flag(flag: &str) -> bool {
    args().iter().any(|s| s.to_lowercase().contains(flag))
}

fn attribute_value<T: FromStr>(attribute_name: &str, default_value: T) -> T {
    match args()
        .iter()
        .find(|s| s.to_lowercase().contains(&format!("{}=", attribute_name)))
    {
        Some(key_value_string) => {
            let key_value: Vec<&str> = key_value_string.split('=').collect();
            if key_value.len() == 2 && !key_value[1].is_empty() {
                T::from_str(key_value[1]).unwrap_or(default_value)
            } else {
                default_value
            }
        }
        None => default_value,
    }
}
