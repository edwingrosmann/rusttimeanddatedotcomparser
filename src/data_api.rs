use std::collections::HashMap;

use chrono::{DateTime, Duration, FixedOffset, Utc};

use crate::kafka_api;
use crate::kafka_api::KafkaAction::Upsert;
use crate::kafka_api::Key;
use crate::mongo_api;
use crate::parse_timeanddate_dot_com::{download_time_data, TimeData};
use crate::settings::{do_cache, do_use_kafka, kafka_topic, sort_strategy, ttl, urls};

/// The Cache-feature assumes a LOCAL MONGODB: TODO: Maybe decouple by using Kafka to send the city-data to storage
/// Storing City Data in MongoDb but only if the 'cache' program argument has been set.
/// Also an optional ttl=480 can be provided: this sets the Time To Live of the stored (cached) data  in [minutes]
pub fn fetch_time_data() -> Vec<TimeData> {
    //Is the time-data to be served up from cache?...
    if do_cache() {
        from_cache(&mut urls())
    } else {
        fresh_data(&urls())
    }
}

fn from_cache(urls: &mut HashMap<String, String>) -> Vec<TimeData> {
    //Read the time-data from the MongoDB...
    let cached = mongo_api::select_by_keys(urls.keys().cloned().collect());

    //Determine which cached data can be re-used...
    let mut valid_data = valid_data(&cached);

    //Print out a cache-report...
    cache_status(&cached, &urls, &valid_data);

    //Do not fetch valid time-data...
    valid_data.iter().for_each(|t| remove_url(urls, &t._id));

    let downloaded = fresh_data(&urls);

    //Compare the Hashes of both cached and downloaded data...
    report_changed_downloaded_data(&cached, &downloaded);

    // Update with- or add fetched time-data...
    downloaded.iter().for_each(|t| valid_data.push(t.clone()));

    //Sort the pages by name...
    valid_data.sort_by_key(|t| t._id.to_string());

    valid_data
}

fn fresh_data(urls: &HashMap<String, String>) -> Vec<TimeData> {
    //Download the time-data...
    let data = download_time_data(sort_strategy(), urls);

    //New data - if required - needs to be transmitted as Kafka-messages...
    optionally_send_kafka_messages(&data);

    //Only store locally when the caching is required and the 'kafka'-feature-flag has not been set...
    optionally_store_locally(&data);

    data
}

///The cache is deemed invalid when:
/// 1) There IS NO Cached-data
/// 2) If the sort-order does not match the required sort-order
/// 3) The number of cached items does NOT match the number of urls
/// 4) NOT All urls-keys are present in the cache
/// 5) ANY cached-item has expired.
///So When there are six URLs and six cached items but the keys do not ALL match, this function will return false.
pub fn cache_status(
    cached_data: &Vec<TimeData>,
    urls: &HashMap<String, String>,
    valid_data: &Vec<TimeData>,
) -> bool {
    let sort_order = sort_strategy();
    let cache_age = cache_age(&cached_data);

    //The cache-validation criteria...
    let sort_order_valid = cached_data.iter().all(|d| sort_order == d.sort);
    let cache_size_valid = cached_data.len() != 0 && cached_data.len() == urls.len();
    let cache_complete = urls
        .keys()
        .all(|key| cached_data.iter().any(|t| t._id.eq(&key.to_string())));
    let cache_age_ok = cache_age <= ttl();

    //Share the love...
    println!(
        "Cache Status Report:\n\tCache age = {};\n\tTTL (Time To Live) = {};\n\tCache Expired: {}.\n\tCache contain correct number of elements: {}.\n\tAll URLs have been cached: {}.\n\tCached data has sort-order '{:?}': {}",
        cache_age, ttl(), !cache_age_ok,
        cache_size_valid, cache_complete, sort_order, sort_order_valid
    );

    println!("Valid Time-Data: {:?}", get_ids(&valid_data));

    //Return the verdict...
    sort_order_valid && cache_size_valid && cache_complete && cache_age_ok
}

fn get_ids(data: &Vec<TimeData>) -> Vec<String> {
    data.iter().map(TimeData::get_id).collect()
}

fn report_changed_downloaded_data(time_data: &Vec<TimeData>, downloaded: &Vec<TimeData>) {
    if !downloaded.is_empty() {
        println!(
            "Downloaded Time-Data NOT same as Cache: {:?}",
            downloaded
                .iter()
                .filter(|fresh| time_data
                    .iter()
                    .any(|cached| fresh._id == cached._id && fresh.hash != cached.hash))
                .map(TimeData::get_id)
                .collect::<Vec<String>>()
        );
    }
}

fn remove_url(urls_to_fetch: &mut HashMap<String, String>, k: &String) {
    urls_to_fetch.remove(k);
}

fn valid_data(map: &Vec<TimeData>) -> Vec<TimeData> {
    //can compare these rfc3339-strings to find the oldest date!
    map.iter().filter(|v| valid(v)).map(|t| t.clone()).collect()
}

fn valid(v: &TimeData) -> bool {
    Utc::now() - last_updated(v) <= ttl() && v.sort == sort_strategy()
}

fn cache_age(map: &Vec<TimeData>) -> Duration {
    //can compare these rfc3339-strings to find the oldest date!
    match map.iter().min_by_key(|x| &x.last_updated) {
        Some(t) => Utc::now() - last_updated(t),
        None => Duration::seconds(0),
    }
}

fn last_updated(x: &TimeData) -> DateTime<Utc> {
    DateTime::<FixedOffset>::parse_from_rfc3339(&x.last_updated)
        .unwrap_or(Utc::now().into())
        .into()
}

fn optionally_store_locally(cache: &Vec<TimeData>) {
    if do_cache() && !do_use_kafka() {
        mongo_api::store(&cache);
    }
}

fn optionally_send_kafka_messages(cache: &Vec<TimeData>) {
    if do_use_kafka() {
        cache.iter().for_each(send_message)
    }
}

fn send_message(v: &TimeData) {
    match kafka_api::write(
        Key {
            name: v.get_id(),
            action: Upsert,
        },
        &v,
        &kafka_topic(),
    ) {
        _ => (),
    }
}
