use html5ever::{parse_document};
use html5ever::tendril::TendrilSink;
use markup5ever_rcdom::RcDom;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub  async fn get_dom(url: &String) -> RcDom {

    parse_document(RcDom::default(), Default::default())
        .from_utf8()
        .read_from(&mut fetch_url_body(url).await.expect("Could not download the Url").as_bytes())
        .unwrap()
}

async fn fetch_url_body(url: &String) -> Result<String> {
    surf::get(url).await?.body_string().await
}
