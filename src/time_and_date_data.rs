use chrono::{FixedOffset, Utc, Weekday};
pub use http::Uri;
use mongodb::bson::Bson;
use serde::de::{Error, Unexpected, Visitor};
use serde::export::Formatter;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::cmp::Ordering;
use std::collections::btree_set::BTreeSet;
use std::collections::hash_map::DefaultHasher;
pub use std::convert::From;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::str;
use std::str::FromStr;
use time;
use String;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct TimeData {
    pub _id: String,
    pub page_uri: UriWrapper,
    pub node_data: String,
    pub city_times: BTreeSet<CityData>,
    pub last_updated: String,
    pub sort: Sort,
    pub hash: String,
    pub count: i32,
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct CityData {
    pub id: i32,
    pub name: String,
    pub time_string: String,
    pub utc_offset: UtcOffset,
    pub is_dls: bool,
    pub url: String,
    pub sort: Sort,
}

#[derive(Debug, Clone)]
pub struct UriWrapper(Uri);

#[derive(Debug, Clone)]
pub struct DayOfWeek(Weekday);

#[derive(Debug, Clone, Hash)]
pub struct UtcOffset(FixedOffset);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Hash)]
pub enum Sort {
    ByName,
    ByOffset,
}

impl PartialEq for Sort {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

impl TimeData {
    pub fn page_uri(&self) -> &Uri {
        &self.page_uri.0
    }

    pub fn from(id: String, url: &String, sort: &Sort) -> Self {
        TimeData {
            _id: id,
            sort: *sort,
            page_uri: UriWrapper::new(url.parse::<Uri>().unwrap()),
            last_updated: Utc::now().to_rfc3339(),
            ..Default::default()
        }
        .add_utc()
    }

    fn add_utc(mut self) -> Self {
        self.city_times.insert(CityData {
            name: "UTC".to_string(),
            url: String::from("https://www.timeanddate.com/time/aboututc.html"),
            sort: self.sort,
            id: -1,
            ..Default::default()
        });
        self
    }

    pub fn update(mut self) -> Self {
        self.count = self.city_times.len() as i32;
        self.last_updated = Utc::now().to_rfc3339();

        //Set the hash; can be used to verify if city-data has changed: e.g. DST-changes
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        self.hash = s.finish().to_string();
        self
    }

    pub fn get_id(&self) -> String {
        self._id.to_owned()
    }
}

impl CityData {
    pub fn from(sort: &Sort) -> Self {
        CityData {
            sort: *sort,
            id: -1,
            ..Default::default()
        }
    }
}

impl UriWrapper {
    pub fn new(uri: Uri) -> UriWrapper {
        UriWrapper(uri)
    }
}

impl Default for UriWrapper {
    #[inline]
    fn default() -> UriWrapper {
        UriWrapper(Uri::default())
    }
}

impl Serialize for UriWrapper {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        Bson::String(self.0.to_string()).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for UriWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        struct UriWrapperVisitor;

        impl<'de> Visitor<'de> for UriWrapperVisitor {
            type Value = UriWrapper;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("Need an URI")
            }

            fn visit_string<E>(self, s: String) -> Result<Self::Value, E>
            where
                E: Error,
            {
                if s.len() > 0 {
                    Ok(UriWrapper(
                        Uri::from_str(&s).expect(format!("Not an URI: {}", &s).as_str()),
                    ))
                } else {
                    Err(Error::invalid_value(Unexpected::Str(&s), &self))
                }
            }
        }
        deserializer.deserialize_identifier(UriWrapperVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> Result<(), <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        unimplemented!()
    }
}

impl Serialize for UtcOffset {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        Bson::String(self.0.to_string()).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for UtcOffset {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        struct UtcOffsetVisitor;

        impl<'de> Visitor<'de> for UtcOffsetVisitor {
            type Value = UtcOffset;

            fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
                formatter.write_str("Need an UtcOffset:")
            }

            fn visit_borrowed_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: Error,
            {
                self.visit_str::<E>(s)
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: Error,
            {
                if s.len() > 0 {
                    Ok(FixedOffset::east(
                        time::UtcOffset::parse(&s.replace(":", ""), "%z")
                            .expect(format!("Not a valid UTC-Offset string: {}", &s).as_str())
                            .as_seconds(),
                    )
                    .into())
                } else {
                    Err(Error::invalid_value(Unexpected::Str(&s), &self))
                }
            }
        }
        deserializer.deserialize_str(UtcOffsetVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> Result<(), <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        unimplemented!()
    }
}

impl Default for Sort {
    fn default() -> Self {
        Self::ByName
    }
}

impl PartialEq for CityData {
    fn eq(&self, other: &CityData) -> bool {
        self.name == other.name
    }
}

impl Eq for CityData {}

impl PartialOrd for CityData {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for CityData {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.sort {
            Sort::ByName => format!("{}-{}", self.name, self.utc_offset.get()).cmp(&format!(
                "{}-{}",
                other.name,
                other.utc_offset.get()
            )),
            Sort::ByOffset => format!("{}-{}", self.utc_offset.get(), self.name).cmp(&format!(
                "{}-{}",
                other.utc_offset.get(),
                other.name
            )),
        }
    }
}

impl DayOfWeek {
    pub fn get(&self) -> Weekday {
        self.0
    }
}

impl From<&str> for DayOfWeek {
    fn from(s: &str) -> Self {
        match &*s.to_uppercase() {
            "MON" => Weekday::Mon.into(),
            "TUE" => Weekday::Tue.into(),
            "WED" => Weekday::Wed.into(),
            "THU" => Weekday::Thu.into(),
            "FRI" => Weekday::Fri.into(),
            "SAT" => Weekday::Sat.into(),
            "SUN" => Weekday::Sun.into(),
            _ => Weekday::Sun.into(),
        }
    }
}

impl From<Weekday> for DayOfWeek {
    fn from(s: Weekday) -> Self {
        DayOfWeek(s)
    }
}

impl UtcOffset {
    pub fn get(&self) -> FixedOffset {
        self.0
    }
}

impl From<FixedOffset> for UtcOffset {
    fn from(d: FixedOffset) -> Self {
        UtcOffset(d)
    }
}

impl Default for UtcOffset {
    fn default() -> Self {
        FixedOffset::east(0).into()
    }
}

impl Hash for TimeData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.sort.hash(state);
        self.city_times.hash(state);
    }
}
impl Hash for CityData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.utc_offset.hash(state);
        self.url.hash(state);
    }
}

#[macro_export]
macro_rules! merge {
    ( $( $more_time_data:expr ),* ) => {
        {
            let mut merged = TimeData::default();
            $(
               merged.city_times = merged.city_times.union(&$more_time_data.unwrap().city_times).cloned().collect();
            )*
           vec!(merged)
        }
    };
}

#[macro_export]
macro_rules! to_vec {
    ( $( $more_time_data:expr ),* ) => {
        {
            let mut vec = Vec::new();
            $(
               vec.push($more_time_data.unwrap());
            )*
            vec
        }
    };
}
