## Introduction
This application is 15.7MB in size and does all of the following:

1. Asynchronously downloads the City-times of an number of web-pages of https://www.timeanddate.com/worldclock/ - as defined in [urls.txt](urls.txt)
1. Shows all these City-times in a Graphical User Interface; Druid.
1. Optionally caches the downloaded City-data in a local MongoDB; add the ```cache``` attribute when starting the application.<br>The cached city-times are used when the application is restarted whilst the cached city-times have not expired yet.
1. Optionally sends downloaded city-data as Kafka-messages; one message per web-page.
1. Shows a RESIZABLE Clock which shows the local time; A Druid Window.
1. Shows a simple animation; Also Druid.

The utility of this application is simple; there is none: The development of it gave however, my teach-yourself-Rust-journey direction and purpose; It taught me all - but not limited to - of the following:

- Idiomatic rust; my earlier code - which was still very Java - has been morphed into functional programming code slowly but surely; still not quite there yet though.
- That the borrow-checker WILL become your trusted friend after a while.
- That I really want to work as a Rust-developer.
- async/await; tokio.
- Downloading and parsing web-pages; extracting data from them and putting it in a data-structure of choice; html5ever
- Displaying this City-times-data in a graphical user interface: Druid.
- Storing this data in a MongoDB
- Sending this data to a Kafka Message Topic
- Creating a Cache; its business logic involves; Time-to-live (TTL) and integrity of the cached data; are all requested pages - but no more - available in the cache; does the sort-order of the cached data match the required sort order.
- Writing unit tests in Rust
- Writing integration tests in Rust
- Running a code coverage tool; grcov.
- Writing documentation.
- Quite a few aspects around Crates.
- That [CLion](https://www.jetbrains.com/clion/) is my preferred IDE; Fantastic code-completion - ALL Crates, Structs and Traits that are in scope are suggested - and it CAN DEBUG! 

Please feel free to make recommendations; pull-request are very practical for that.

It is my ambition to become an OK Rust Developer. The creation of this application/crate was my first step towards that goal.

#### The Kafka-time-data-messages-read-and-store-in-MongoDB-Microservice
I have also developed a [Micro-service that reads the Kafka-messages and stores them in a MongoDB](https://bitbucket.org/edwingrosmann/timedata_store); That beauty is just 12.2MB in size; Run it next to this application with the ```kafka``` feature flag set:

Run this application as:<br>
```cargo run --release -- kafka cache```<br>
Run the other message-receiver-micro-service as:<br>
```cargo run --release```

##### Run with a **MongoDb-backed-cache** with a Time-To-Live of  8 hours...
```
 cargo run --release -- use_cache ttl=480 order-by-offset
 cargo run --release -- use-cache ttl=480 order-by-name
```
##### Throw  **Kafka** in the mix...
Only freshly downloaded data is sent; so when the time-data is served-up from the cache, NO Kafka-messages are produced.
```
 cargo run --release -- cache kafka kafka-topic=schmoppic
 cargo run --release -- kafka kafka-topic=schmoppic
```
#### The use of the 'kafka' and 'cache' attributes...
The application behaviour of the **feature-flags** '**cache**' and '**kafka**' is as follows:

_Attribute_|**cache**|**kafka**|**cache + kafka**|Neither
---|---|---|---|---
_Effect_|Use cache (Non-expired data from MongoDB) / Store downloaded Time-data directly in MongoDB | Always download time-data and send it as Kafka-messages | Use Cache. Downloaded Time-data is NOT stored directly in MongoDB; it is sendt as Kafka0messages instead.|The Time-data is always downloaded. Nothing else :-) 

Feature flag **print-node-data** - if NOT omitted - hammers the log is with Node-data as it occurs during the web-page-parsing process.

#### All attributes are optional.
_Attribute_|_Default Value_|_Possible Values_
---|---|---
**ttl**|480 [s]|0..max-64-bit-integer
**kafka-topic**|time-data|Any string that behooves Kafka
**sort-order**|by-name| **_by-name_**: Time data is ordered by City-name, then by UTC-offset;<br>**_by-offset_**: First order by UTC-offset then City-name

#### The urls.txt file
The [urls.txt](urls.txt) must sit in the directory from where the application is run.
Lines can be commented-out with a ```#``` or a ```//```.<br>
Each Line contains a name/value pair; The first ```'='```-character separates them.<br>
The name is the MongoDB and Kafka-message **Key**. <br>
The value is the timeanddate.com city-data-URL

These are the default values...
```
Popular Cities=https://www.timeanddate.com/worldclock/?low=4
Europa=https://www.timeanddate.com/worldclock/?continent=europe

Africa=https://www.timeanddate.com/worldclock/?continent=africa
Australasia=https://www.timeanddate.com/worldclock/?continent=australasia
North Americas=https://www.timeanddate.com/worldclock/?continent=namerica
South Americas=https://www.timeanddate.com/worldclock/?continent=samerica
Asia=https://www.timeanddate.com/worldclock/?continent=asia
```
## Application Features in detail

#### The Date-And-Time.com Druid-UI
![](pictures/date_and_time_ui.png)

#### Some Resizeable Clock
![](pictures/clock_ui.png)

#### An Animation
![](pictures/animation.png)

## Code Coverage

#### Please check out the [Integration Tests instructions](tests/README.md)  if you want to try run 'rgcov' Code Coverage on this crate.

#### First install grcov
```
cargo install grcov
```
#### Then run the code coverage in a terminal
```
#Only once...
export CARGO_INCREMENTAL=0;
export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=unwind";
export RUSTDOCFLAGS="-Cpanic=abort"

#in crate root...
cargo +nightly clean -p rusttimeanddatedotcomparser
cargo +nightly test --test mod  -- tests  cache
grcov ./target/debug/  -t html --llvm --branch --ignore-not-existing -o ./target/debug/coverage/
```
#### View Result at ```/target/debug/coverage/index.html```
Here a result BEFORE the integration tests could be run unattended...
![](pictures/Screenshot_2020-08-20%20Grcov%20report%20—src.png)
A week later the integration tests can run without human intervention: over-all coverage of **94.7%**; Nothing to sneeze at. But if you must; please cover your face :-) 
![](pictures/Screenshot_2020-08-26%20Grcov%20report%20—src.png)


