use r::kafka_api::create_topic;
use rusttimeanddatedotcomparser as r;
mod util;
use util::{args_for, delete_all_mongodb_documents};

const TEST_TOPIC: &str = "integration-test";
#[test]
fn main_test() {
    //create the integration-test topic upfront, so the receiving microservice has time to spin up...
    create_topic(TEST_TOPIC).unwrap();

    //delete all MongoDB-documents...
    delete_all_mongodb_documents();

    r::main(
        true,
        args_for(vec![
            "urls-file=tests/integration_test_urls.txt",
            "cache",
            "ttl=480",
            "order_by_offset",
            "use-kafka",
            &("topic=".to_owned() + TEST_TOPIC),
        ]),
    );

    // This is the best e2e-test possible; Also spin up the ```timedata_store``` MicroService!
    // E.g. ```cargo run -- topic=integration-test group=integration-test```
    // See to it that on the 'integration-test'-topic messages are produced and that these are consumed and stored in the MongoDB...
    r::main(
        true,
        args_for(vec![
            "urls-file=tests/integration_test_urls.txt",
            "cache",
            "ttl=123",
            "order-by-name",
        ]),
    );

    //In case of an e2e-test: do NOT delete the kafka-topic, so that it can be inspected...

    //Read from cache...
    r::main(
        true,
        args_for(vec![
            "urls-file=tests/integration_test_urls.txt",
            "cache",
            "order-name",
        ]),
    );

    r::main(
        true,
        args_for(vec![
            "urls-file=tests/integration_test_urls.txt",
            "cache",
            "ttl=0",
            "order-name",
            "print-node-data",
            "use-kafka",
            &("topic=".to_owned() + TEST_TOPIC),
        ]),
    );

    //This should restore the 'normal urls.txt' data-set; It's directly wtriting to the MongoDB; no kafka set...
    r::main(true, args_for(vec!["cache", "ttl=0"]));

    util::delete_kafka_topic(TEST_TOPIC);
}
