use bson::Document;
use kafka::client::KafkaClient;
use mongodb::Client;
use std::process::Command as Cmd;

pub fn args_for(args: Vec<&str>) -> Vec<String> {
    args.iter().map(|s| s.to_string()).collect()
}

pub fn delete_kafka_topic(topic: &str) {
    //Only continue when the topic does NOT yet exist...
    let mut client = KafkaClient::new(vec!["localhost:9092".to_owned()]);
    client.load_metadata_all().unwrap();

    if client.topics().contains(topic) {
        match Cmd::new("kafka-topics")
            .arg("--delete")
            .arg(format!("--topic={}", topic).as_str())
            .arg("--bootstrap-server=localhost:9092")
            .status()
        {
            Ok(_) => {
                println!("Deleted Kafka-topic: '{}'", topic);
            }
            Err(e) => {
                println!("Error deleting Kafka-topic '{}': {}", topic, e);
            }
        }
    } else {
        println!("Kafka-topic already deleted.'{}'", topic);
    }
}

pub fn delete_all_mongodb_documents() {
    delete_all_documents(
        create_mongo_client("localhost").expect("Could not create MongoDb Client"),
    )
    .expect("Could not delete all MongoDb test/time_data-Documents.");
}

#[tokio::main]
async fn delete_all_documents(client: Client) -> mongodb::error::Result<()> {
    let collection = client.database("test").collection("city_data");
    collection.delete_many(Document::default(), None).await?;
    Ok(())
}

#[tokio::main]
async fn create_mongo_client(host: &str) -> mongodb::error::Result<Client> {
    Ok(Client::with_uri_str(format!("mongodb://{}:27017", host).as_str()).await?)
}
